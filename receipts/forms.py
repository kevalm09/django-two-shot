from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
            "purchaser",
        ]


class CreateCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
            "owner",
        ]


class CreateAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
            "owner",
        ]
