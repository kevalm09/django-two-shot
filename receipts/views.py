from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)

# Create your views here.


@login_required
def the_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "the_receipt": receipt,
    }
    return render(request, "receipts/receipts_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = CreateReceiptForm
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category}
    return render(request, "receipts/categories.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"accounts": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
